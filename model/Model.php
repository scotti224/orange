<?php

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\AnnotationRegistry;

class model {

    public function doctrine() {
        $paths = array(__DIR__ . "/model");
        $config = Setup::createAnnotationMetadataConfiguration($paths, false);
        return EntityManager::create($this->getSettings(), $config);
    }

    private function getSettings() {
        return array(
            'driver' => 'pdo_mysql',
            'user' => 'root',
            'password' => '28590133',
            'dbname' => 'api',
        );
    }

    public function getModel($name) {

        $path = explode('/', $name);

        $directory = ucfirst($path[0]);
        $model = ucfirst($path[1]);

        $pathModel = __DIR__ . '/' . $directory . '/' . $model . 'Model.php';

        if (file_exists($pathModel)) {

            require_once $pathModel;

            if (class_exists($model)) {
                return new $model;
            }
        }
    }

}
