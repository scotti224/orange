<?php

/**
 * @Entity @Table(name="device")
 * */
class Device {

    /**
     * @Column(name="id", type="integer")
     * @Id
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Column(name="series", type="integer")
     * @var int 
     */
    protected $series;

    /**
     * @Column(name="create_date", type="date")
     * @var date
     */
    protected $createDate;

    /**
     * @Column(name="update_date", type="date")
     * @var string
     */
    protected $updateDate;

    /**
     * @Column(name="flag", type="integer")
     * @var int
     */
    protected $flag;

    public function getId() {
        return $this->id;
    }

    public function setSeries($series) {
        $this->series = $series;
    }

    public function getSeries() {
        return $this->series;
    }

    public function setCreateDate($createDate) {
        $this->createDate = $createDate;
    }

    public function getCreateData() {
        $this->createDate = $createDate;
    }

    public function setUpdateDate($updateDate) {
        $this->updateDate = $updateDate;
    }

    public function getUpdateDate() {
        return $this->updateDate;
    }

    public function setFlag($flag) {
        $this->flag = $flag;
    }

    public function getFlag() {
        return $this->flag;
    }

}
