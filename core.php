<?php

namespace Cms;

final class Core {

    private $controller = '';
    private $method = '';
    private $param = [];

    public function __construct() {
        session_start();
        ob_start();
    }

    /**
     * Set controller and params from method GET site
     * @param array $data
     */
    public function setData($data) {

        $data = explode('/', $data);
        $this->setController($data['0']);
        unset($data['0']);
        $this->setMethod($data['1']);
        unset($data['1']);
        $this->setParam($data);
    }

    /**
     * Set controller
     * @param string $name
     */
    public function setController($name) {
        $this->controller = $name;
    }

    /**
     * Get controller
     * @return string
     */
    public function getController() {
        return $this->controller;
    }

    /**
     * Set method
     * @param string $name
     */
    public function setMethod($name) {
        $this->method = $name;
    }

    /**
     * Get method
     * @return string
     */
    public function getMethod() {
        return $this->method;
    }

    /**
     * set Params
     * @param array $data
     */
    public function setParam($data) {
        $this->param = $data;
    }

    /**
     * Get Params
     * @return array
     */
    public function getParam() {
        return $this->param;
    }

    /**
     * Run system
     */
    public function run() {

        $controller = ucfirst($this->getController()) . "Controller";
        $method = $this->getMethod() . 'Action';
        $param = $this->getParam();
        $pathController = 'controller/' . $controller . '.php';
        if (file_exists($pathController)) {

            require_once $pathController;

            if (class_exists($controller)) {
                $object = new $controller;

                if (method_exists($object, $method)) {

                    if (count($param) > 0) {
                        
                        call_user_func_array([$object, $method], $param);
                        
                    } else {
                        $object->$method();
                    }
                }
            }
        }
    }
    
    public function __desctruct(){
        
        ob_end_flush();
    }
}