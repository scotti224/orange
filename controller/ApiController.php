<?php

class ApiController extends Controller {
    
    public function testAction() {
        
        $model = $this->Resource()->getModel('api/device');
        $model->setSeries(2);
        $model->setCreateDate(new \DateTime('10-12-2017'));
        $model->setUpdateDate(new \DateTime('10-12-2017'));
        $model->setFlag(12333);
        $entityManager = $this->Resource()->doctrine();
        $entityManager->persist($model);
        $entityManager->flush();
        echo $model->getFlag();
    }

}
