<?php

require_once __DIR__.'/model/Model.php';

class Controller {
    
    /**
     * Get resource class model
     * @return \model object
     */
    public function Resource() {
        
        return new model();
        
    }

}
